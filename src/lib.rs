#![feature(try_trait)]
#![feature(result_flattening)]
#[macro_use]
extern crate gdnative;
extern crate interpolation;

use gdnative::prelude::*;

mod menus;
mod player;
mod save_data;
mod scene_manager;
mod targets;
mod utils;

fn init(handle: InitHandle) {
	handle.add_class::<player::Player>();
	handle.add_class::<player::PlayerSettings>();
	handle.add_class::<player::Radar>();
	handle.add_class::<targets::TargetManager>();
	handle.add_class::<targets::Target>();
	handle.add_class::<scene_manager::SceneManager>();
	handle.add_class::<menus::PauseMenu>();
	handle.add_class::<menus::VolumeMenu>();
	handle.add_class::<menus::VolumeSlider>();
	handle.add_class::<menus::InputMenu>();
	handle.add_class::<menus::GraphicsMenu>();
	handle.add_class::<save_data::Settings>();
	handle.add_class::<save_data::InputMapSettings>();
	handle.add_class::<save_data::GraphicsSettings>();
}

godot_gdnative_init!();
godot_nativescript_init!(init);
godot_gdnative_terminate!();
