pub use std::f64::consts::PI;
use std::fmt::Debug;

pub const TAU: f64 = PI * 2.0f64;

pub trait GDExpect<T> {
	fn gdexpect(self, message: &str) -> T;
	fn gdunwrap(self) -> T;
}

impl<T> GDExpect<T> for Option<T> {
	#[inline(always)]
	fn gdexpect(self, message: &str) -> T {
		match self {
			Some(t) => t,
			None => {
				godot_error!("{}", message);
				self.expect(message)
			}
		}
	}
	
	#[inline(always)]
	fn gdunwrap(self) -> T {
		let msg = "`gdunwrap()` called on a None";
		match self {
			Some(t) => t,
			None => {
				godot_error!("{}", msg);
				self.unwrap()
			}
		}
	}
}

impl<T, E: Debug> GDExpect<T> for Result<T, E> {
	#[inline(always)]
	fn gdexpect(self, message: &str) -> T {
		match self {
			Ok(t) => t,
			Err(e) => {
				godot_error!("{}: {:?}", message, e);
				Result::<T, E>::from(Err(e)).expect(message)
			}
		}
	}
	
	#[inline(always)]
	fn gdunwrap(self) -> T {
		match self {
			Ok(t) => t,
			Err(e) => {
				godot_error!("{:?}", e);
				Result::<T, E>::from(Err(e)).unwrap()
			}
		}
	}
}
//
// pub trait Lerp {
// 	fn lerp(self, other: Self, t: Self) -> Self;
// }
//
// impl Lerp for f64 {
// 	#[inline]
// 	fn lerp(self, other: f64, t: f64) -> f64 {
// 		(other - self) * t + self
// 	}
// }
//
// impl Lerp for f32 {
// 	#[inline]
// 	fn lerp(self, other: f32, t: f32) -> f32 {
// 		(other - self) * t + self
// 	}
// }

pub(crate) const FIRE: &str = "fire";
pub(crate) const SCOPE_IN: &str = "scope_in";
pub(crate) const MOVE_FORWARD: &str = "move_forward";
pub(crate) const MOVE_BACK: &str = "move_back";
pub(crate) const MOVE_LEFT: &str = "move_left";
pub(crate) const MOVE_RIGHT: &str = "move_right";
pub(crate) const LOOK_UP: &str = "look_up";
pub(crate) const LOOK_DOWN: &str = "look_down";
pub(crate) const LOOK_LEFT: &str = "look_left";
pub(crate) const LOOK_RIGHT: &str = "look_right";
pub(crate) const JUMP: &str = "jump";

pub(crate) const ACTIONS: [&str; 11] = [
	FIRE,
	SCOPE_IN,
	MOVE_FORWARD,
	MOVE_BACK,
	MOVE_LEFT,
	MOVE_RIGHT,
	LOOK_UP,
	LOOK_DOWN,
	LOOK_LEFT,
	LOOK_RIGHT,
	JUMP,
];
