use gdnative::api::{AudioServer, HSlider, VBoxContainer};
use gdnative::prelude::*;
use crate::scene_manager::SceneManager;
use gdnative::GodotResult;
use std::ops::Deref;

#[derive(NativeClass, Default, Debug)]
#[inherit(VBoxContainer)]
pub(crate) struct VolumeMenu;

#[methods]
impl VolumeMenu {
	fn new(_owner: &VBoxContainer) -> Self {
		VolumeMenu
	}
	
	#[export]
	fn save(&self, owner: &VBoxContainer) -> GodotResult {
		SceneManager::warn(owner, "Sorry, saving volume settings is not yet implemented.\
Go bug Waridley about it!");
		
		unimplemented!("Saving volume settings is not yet implemented!") //TODO
	}
	
	#[export]
	fn load(&self, owner: &VBoxContainer) -> GodotResult{
		SceneManager::warn(owner, "Sorry, loading volume settings is not yet implemented.\
Go bug Waridley about it!");
		unimplemented!("Loading volume settings is not yet implemented!") //TODO
	}
}

#[derive(NativeClass, Debug)]
#[inherit(HSlider)]
pub(crate) struct VolumeSlider {
	#[property]
	bus_name: String,
}

impl Default for VolumeSlider {
	fn default() -> Self {
		VolumeSlider {
			bus_name: String::from("Master"),
		}
	}
}

#[methods]
impl VolumeSlider {
	fn new(_owner: &HSlider) -> Self {
		VolumeSlider::default()
	}
	
	#[export]
	fn _ready(&self, owner: &HSlider) {
		let asrv = AudioServer::godot_singleton();
		
		let idx = asrv.get_bus_index(self.bus_name.clone());
		let vol = asrv.get_bus_volume_db(idx);
		owner.set_block_signals(true);
		owner.set_value(vol);
		owner.set_block_signals(false);
	}
	
	#[export]
	fn on_value_changed(&self, owner: &HSlider, value: f64) -> GodotResult {
		let asrv = AudioServer::godot_singleton();
		let idx = asrv.get_bus_index(&self.bus_name);
		if value < ((-36.0 + f32::EPSILON) as f64) {
			asrv.set_bus_mute(idx, true);
		} else {
			asrv.set_bus_mute(idx, false);
			asrv.set_bus_volume_db(idx, value);
		}
		SceneManager::settings_changed(&*unsafe { owner.find_parent("Audio")?.assume_safe() })
	}
	
}
