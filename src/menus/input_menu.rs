use crate::player::{Player, PlayerSettings};
use crate::save_data::InputMapSettings;
use crate::utils::*;
use gdnative::api::{GridContainer, HBoxContainer, HSlider, InputEventJoypadButton, InputEventJoypadMotion, InputEventMouseButton, InputEventMouseMotion, InputMap, Resource, ResourceSaver, VBoxContainer, PanelContainer, VSeparator};
use gdnative::prelude::*;
use heck::TitleCase;
use crate::scene_manager::SceneManager;
use gdnative::GodotResult;

#[derive(NativeClass, Default, Debug)]
#[inherit(VBoxContainer)]
#[user_data(LocalCellData<InputMenu>)]
pub(crate) struct InputMenu {
	button_being_bound: Option<Ref<Button>>,
	action_being_bound: Option<GodotString>,
	event_being_replaced: Option<Ref<InputEvent>>,
	delete_mode: bool,
}

#[methods]
impl InputMenu {
	fn new(_owner: &VBoxContainer) -> Self {
		Self::default()
	}
	
	#[export]
	fn _ready(&self, owner: &VBoxContainer) -> GodotResult {
		owner.set_process_input(false);
		Self::update_sliders(owner)?;
		
		self.load_input_map_settings(owner, "res://test_saves/saved_input_map.tres".into())
	}
	
	fn update_sliders(owner: &VBoxContainer) -> GodotResult {
		unsafe {
			find_player(owner)?.map(|player, _owner| {
				let mut settings = PlayerSettings::default();
				player.settings.as_ref()?.assume_safe()
					.map(|player_settings, _owner| {
						settings.sensitivity = player_settings.sensitivity;
						settings.scoped_sens_ratio = player_settings.scoped_sens_ratio;
					}).gdexpect("Failed to map instance of PlayerSettings!");
				let sens = settings.sensitivity;
				let scoped_sens_ratio = settings.scoped_sens_ratio;
				
				let slider = owner.find_node("sens_slider", true, true)
					.gdexpect("Couldn't find SensSlider!").assume_safe()
					.cast::<HSlider>().gdexpect("SensSlider isn't an HSlider!");
				slider.set_block_signals(true);
				slider.set_value(sens as f64);
				slider.set_block_signals(false);
				
				let slider = owner.find_node("rel_sens_slider", true, true)
					.gdexpect("Couldn't find RelSensSlider!").assume_safe()
					.cast::<HSlider>().gdexpect("RelSensSlider isn't an HSlider!");
				slider.set_block_signals(true);
				slider.set_value(scoped_sens_ratio as f64);
				slider.set_block_signals(false);
				Ok(())
			}).unwrap_or_else(|e| {
				godot_error!("Failed to map instance of Player! {:#?}", e);
				Err(GodotError::Failed)
			})
		}
	}
	
	#[export]
	fn adjust_mouse_sensitivity(&self, owner: &VBoxContainer, value: f64) -> GodotResult {
		owner.map_player_settings_mut(|settings, _owner| {
			settings.sensitivity = value;
		}).map_err(|_| GodotError::ScriptFailed)?;
		// SceneManager::emit_signal(owner, "settings_changed", &[
		// 	Variant::from_object(unsafe { owner.assume_shared() }),
		// 	Variant::from_str("save"),
		// ])
		SceneManager::settings_changed(owner)
	}
	
	#[export]
	fn adjust_scoped_sens_ratio(&self, owner: &VBoxContainer, value: f64) -> GodotResult {
		owner.map_player_settings_mut(|settings, _owner| {
			settings.scoped_sens_ratio = value;
		}).map_err(|_| GodotError::ScriptFailed)?;
		// SceneManager::emit_signal(owner, "settings_changed", &[
		// 	Variant::from_object(unsafe { owner.assume_shared() }),
		// 	Variant::from_str("save"),
		// ])
		SceneManager::settings_changed(owner)
	}
	
	#[export]
	fn quit(&self, owner: &VBoxContainer) -> GodotResult {
		SceneManager::emit_signal(owner, "quit", &[
			Variant::from_object(unsafe { owner.assume_shared() }),
			Variant::from_str("save"),
		])
	}
	
	#[export]
	fn on_continue_pressed(&self, owner: &VBoxContainer) {
		owner.hide()
	}
	
	#[export]
	fn on_binding_button_toggled(
		&mut self,
		owner: &VBoxContainer,
		pressed: bool,
		button: Ref<Button>,
		action: GodotString,
		event: Option<Ref<InputEvent>>,
	) -> GodotResult {
		let button = unsafe { button.assume_safe() };
		
		if self.delete_mode {
			button.queue_free();
			if let Some(event) = event {
				InputMap::godot_singleton().action_erase_event(action, event);
			}
			SceneManager::emit_signal(owner, "settings_changed", &[
				Variant::from_object(unsafe { owner.assume_shared() }),
				Variant::from_str("save"),
			])
		} else {
			button.release_focus();
			if pressed {
				owner.set_process_input(true);
				button.set_text("...");
				self.button_being_bound = Some(unsafe { button.assume_shared() });
				self.action_being_bound = Some(action.new_ref());
				self.event_being_replaced = event;
				SceneManager::emit_signal(owner, "settings_changed", &[
					Variant::from_object(unsafe { owner.assume_shared() }),
					Variant::from_str("save"),
				])
			} else if let Some(event) = event {
				self.cancel_binding(owner);
				button.set_text(event.description());
				Ok(())
			} else {
				godot_warn!("Event was None!");
				Err(GodotError::DoesNotExist)
			}
		}
	}
	
	#[export]
	fn add_binding(
		&self,
		owner: &VBoxContainer,
		action: GodotString,
		grid: Ref<GridContainer>,
		add_btn: Ref<Button>,
	) -> GodotResult {
		unsafe {
			add_btn.assume_safe().release_focus();
			let button = new_binding_button(
				owner,
				action.new_ref(),
				None,
				"Press any button"
			);
			button.assume_safe().set_pressed(true);
			grid.assume_safe().add_child(button, true);
		}
		SceneManager::emit_signal(owner, "settings_changed", &[
			Variant::from_object(unsafe { owner.assume_shared() }),
			Variant::from_str("save"),
		])
	}
	
	#[export]
	fn _input(&mut self, owner: &VBoxContainer, event: Ref<InputEvent>) -> GodotResult {
		let input_map = InputMap::godot_singleton();
		
		unsafe { owner.get_tree()?.assume_safe() }.set_input_as_handled();
		
		if let Some(button) = self.button_being_bound {
			
			let event = match event.try_cast::<InputEventMouseMotion>() {
				Ok(_) => return Ok(()),
				Err(e) => e,
			};
			
			let event = match event.try_cast::<InputEventKey>() {
				Ok(e) => unsafe {
					if e.assume_safe().scancode() == gdnative::api::GlobalConstants::KEY_ESCAPE {
						if let Some(event) = self.event_being_replaced.as_ref() {
							button.assume_safe().set_text(event.clone().description());
						} else {
							button.assume_safe().queue_free();
						}
						self.cancel_binding(owner);
						return Ok(())
					}
					e.upcast::<InputEvent>()
				}
				Err(e) => e,
			};
			
			let event = match event.try_cast::<InputEventJoypadMotion>() {
				Ok(e) => unsafe {
					// Don't be too sensitive when modifying InputMap, no matter what deazone usually is.
					if e.assume_safe().axis_value().abs() < 0.7 {
						// owner.get_tree().gdunwrap().assume_safe().set_input_as_handled();
						return Ok(())
					}
					e.upcast::<InputEvent>()
				}
				Err(e) => e,
			};
			
			let action = self.action_being_bound.as_ref()?;
			
			if let Some(prev_event) = self.event_being_replaced.as_ref() {
				input_map.action_erase_event(action.new_ref(), prev_event.clone());
			}
			input_map.action_add_event(
				action.new_ref(),
				event.clone(),
			);
			
			let shared_owner = unsafe { owner.assume_shared() };
			let button = unsafe { button.assume_safe() };
			button.set_text(event.clone().description());
			button.disconnect(
				"toggled",
				shared_owner,
				"on_binding_button_toggled",
			);
			let args = VariantArray::new();
			args.push(button);
			args.push(action.new_ref());
			args.push(event.clone());
			button.connect(
				"toggled",
				shared_owner,
				"on_binding_button_toggled",
				args.into_shared(),
				1,
			).unwrap_or_else(|e| {
				godot_error!("Failed to connect toggled signal for {}! {:#?}", action.new_ref(), e)
			});
			
			self.cancel_binding(owner);
			Ok(())
		} else {
			godot_warn!("Processing input while button_being_bound is None!");
			Err(GodotError::DoesNotExist)
		}
	}
	
	fn cancel_binding(&mut self, owner: &VBoxContainer) {
		owner.set_process_input(false);
		if let Some(prev_button) = self.button_being_bound {
			let btn = unsafe { prev_button.assume_safe() };
			btn.set_block_signals(true);
			btn.set_pressed(false);
			btn.set_block_signals(false);
		}
		self.button_being_bound = None;
		self.action_being_bound = None;
		self.event_being_replaced = None
	}
	
	#[export]
	fn set_delete_mode(&mut self, _owner: &VBoxContainer, value: bool) {
		self.delete_mode = value;
	}
	
	#[export]
	fn save(&self, owner: &VBoxContainer) -> GodotResult {
		#[cfg(debug_assertions)]
			let save_path = "res://test_saves/saved_player_settings.tres";
		#[cfg(not(debug_assertions))]
			let save_path = "user://settings/player_1/settings.res";
		
		find_player(owner)?.map(|player, _owner| {
			if let Some(instance) = &player.settings {
				let res = instance.base();
				ResourceSaver::godot_singleton()
					.save(save_path, res, 0)
					.gdexpect("Couldn't save PlayerSettings!")
			}
		}).gdexpect("Couldn't map instance of Player!");
		
		#[cfg(debug_assertions)]
			let save_path = "res://test_saves/saved_input_map.tres";
		#[cfg(not(debug_assertions))]
			let save_path = "user://settings/input_map.res";
		
		let script = ResourceLoader::godot_singleton()
			.load("res://scripts/InputMapSettings.gdns", "NativeScript", false)
			.gdexpect("Couldn't load InputMapSettings script!");
		
		let res = Resource::new();
		res.set_script(script);
		res.cast_instance::<InputMapSettings>()
			.gdexpect("Couldn't cast instance of InputMapSettings!")
			.map(|script, res| {
				let input_map = unsafe { script.input_map.new_ref().assume_unique() };
				for action in &ACTIONS {
					let bindings = InputMap::godot_singleton().get_action_list(action);
					input_map.insert(action.to_variant(), bindings);
				}
				ResourceSaver::godot_singleton().save(save_path, unsafe { res.assume_shared() }, 0, )
					.gdexpect("Couldn't save Input bindings!");
			}).gdexpect("Couldn't map instance of InputMapSettings!");
		SceneManager::emit_signal(owner, "settings_saved", &[])
	}
	
	#[export]
	fn load_input_map_settings(&self, owner: &VBoxContainer, path: GodotString) -> GodotResult {
		match ResourceLoader::godot_singleton().load(path, "Resource", true) {
			Some(res) => unsafe {
				res.cast_instance::<InputMapSettings>()
					.gdexpect("Failed to cast")
					.assume_safe()
			}.map(|script, _owner| {
				let input = InputMap::godot_singleton();
				for (key, value) in script.input_map.iter() {
					input.action_erase_events(
						key.try_to_godot_string()
							.gdexpect("Couldn't convert key to a GodotString"),
					);
					for event in value.try_to_array()
							.gdexpect(&*format!(
								"Couldn't convert {:#?} to a VariantArray",
								value
							)).iter() {
						let event = event.to_variant().try_to_object::<InputEvent>().gdexpect(
							&*format!("Couldn't convert {:#?} to an InputEvent", event),
						);
						input.action_add_event(key.try_to_godot_string()?, event);
					}
				}
				Ok(())
			}).map_err(|_| GodotError::ScriptFailed).flatten(),
			None => {
				self.load_defaults(owner)?;
				self.save(owner)
			}
		}?;
		reload_actions(owner)
	}
	
	#[export]
	fn load_defaults(&self, owner: &VBoxContainer) -> GodotResult {
		find_player(owner)?.map_mut(|player, _owner| {
			player.settings = ResourceLoader::godot_singleton()
				.load("res://default_player_settings.tres", "Resource", false)?
				.cast_instance::<PlayerSettings>();
			player.settings.as_ref().map(|_|())
		}).gdexpect("Couldn't map instance of Player!");
		Self::update_sliders(owner)?;
		
		InputMap::godot_singleton().load_from_globals();
		reload_actions(owner)?;
		SceneManager::emit_signal(owner, "settings_changed", &[
			Variant::from_object(unsafe { owner.assume_shared() }),
			Variant::from_str("save"),
		])
	}
	
}

struct ActionBindingEntryNode {
	pub panel: Ref<PanelContainer, Unique>,
	pub hbox: Ref<HBoxContainer, Shared>,
	pub label: Ref<Label, Shared>,
	pub grid: Ref<GridContainer, Shared>,
	pub add_button: Ref<Button, Shared>,
}

impl ActionBindingEntryNode {
	fn new(owner: &Node, action: &str) -> Self {
		let panel = PanelContainer::new();
		#[cfg(debug_assertions)] panel.set_name(format!("{}_panel", action));
		
		let hbox = HBoxContainer::new();
		#[cfg(debug_assertions)] hbox.set_name(format!("{}_hbox", action));
		
		let label = Label::new();
		label.set_text(action.to_title_case() + " ");
		#[cfg(debug_assertions)] label.set_name(format!("{}_label", action));
		label.set_custom_minimum_size(Vector2::new(140.0, 0.0));
		label.set_align(Label::ALIGN_RIGHT);
		let label = label.into_shared();
		hbox.add_child(label, false);
		
		hbox.add_child(VSeparator::new(), false);
		
		let grid = GridContainer::new();
		#[cfg(debug_assertions)] grid.set_name(format!("{}_grid", action));
		grid.set_columns(6);
		grid.set_h_size_flags(Control::SIZE_EXPAND_FILL);
		let grid = grid.into_shared();
		hbox.add_child(grid, false);
		
		hbox.add_child(VSeparator::new(), false);
		
		let add_button = Button::new();
		#[cfg(debug_assertions)] add_button.set_name(format!("add_{}_btn", action));
		add_button.set_text(" + ");
		add_button.set_v_size_flags(Control::SIZE_SHRINK_CENTER);
		add_button.set_custom_minimum_size(Vector2::new(25.0, 25.0));
		let add_button = add_button.into_shared();
		let args = VariantArray::new();
		args.push(action);
		args.push(grid);
		args.push(add_button);
		unsafe {
			add_button.assume_safe()
				.connect(
					"pressed",
					owner.assume_shared(),
					"add_binding",
					args.into_shared(),
					0,
				)
				.unwrap_or_else(|e| {
					godot_error!("Failed to connect toggled signal for {}! {:#?}", action, e)
				});
		}
		hbox.add_child(add_button, false);
		
		let hbox = hbox.into_shared();
		
		panel.add_child(hbox, false);
		
		Self { panel, hbox, label, grid, add_button, }
	}
}

fn find_player<'a>(owner: &Node) -> Result<RefInstance<'a, Player, Shared>, GodotError> {
	unsafe {
		Ok(owner
			.get_tree()?.assume_safe()
			.current_scene()?.assume_safe()
			.find_node("Player", true, false)?.assume_safe()
			.cast::<KinematicBody>().gdexpect("Player isn't a KinematicBody!")
			.cast_instance::<Player>()?)
	}
}

trait MapPlayerSettingsMut {
	fn map_player_settings_mut<F, U>(&self, func: F) -> Result<U, GodotError>
	where F: FnOnce(&mut PlayerSettings, TRef<'_, Resource, Shared>) -> U;
}

impl MapPlayerSettingsMut for Node {
	
	fn map_player_settings_mut<F, U>(&self, func: F) -> Result<U, GodotError>
	where F: FnOnce(&mut PlayerSettings, TRef<'_, Resource, Shared>) -> U {
		find_player(self)?.map_mut(|player, _owner| {
			let instance = player.settings.as_ref()?;
			Ok(unsafe { instance.assume_safe() }.map_mut(|settings, owner| {
				func(settings, owner)
			}).map_err(|_| GodotError::ScriptFailed)?)
		}).map_err(|_| GodotError::ScriptFailed)?
	}
	
}

fn load_actions(owner: &VBoxContainer, vbox: TRef<VBoxContainer>) -> GodotResult {
	for action in &ACTIONS {
		let entry = ActionBindingEntryNode::new(owner, action);
		populate_action_bindings(owner, action, entry.grid)?;
		vbox.add_child(entry.panel.into_shared(), false);
	}
	Ok(())
}

fn reload_actions(owner: &VBoxContainer) -> GodotResult {
	unsafe {
		let scrolling_vbox = owner
			.find_node("input_map_vbox", true, true)
			.gdexpect("Couldn't find input_map_vbox node!")
			.assume_safe()
			.cast::<VBoxContainer>()
			.gdexpect("input_map_vbox isn't a VBoxContainer!");
		
		for child in scrolling_vbox.get_children().iter() {
			let child = child.try_to_object::<Node>()?.assume_safe();
			child.queue_free();
		}
		load_actions(owner, scrolling_vbox)
	}
}

fn populate_action_bindings(owner: &VBoxContainer, action: &str, grid: Ref<GridContainer, Shared>)
	-> GodotResult{
	let input_map = InputMap::godot_singleton();
	
	let bindings = input_map.get_action_list(&*action);
	for variant in bindings.iter() {
		let event = variant.try_to_object::<InputEvent>()?;
		let text = event.clone().description();
		
		let button = new_binding_button(owner, &*action, Some(event), text);
		unsafe { grid.assume_safe() }.add_child(button, true);
	}
	Ok(())
}

fn new_binding_button(
	owner: &VBoxContainer,
	action: impl Into<GodotString>,
	event: Option<Ref<InputEvent>>,
	text: impl Into<GodotString>,
) -> Ref<Button, Shared> {
	unsafe {
		let text = text.into();
		let button = Button::new();
		button.set_text(text.new_ref());
		button.set_custom_minimum_size(Vector2::new(125.0, 0.0));
		let button = button.assume_shared();
		let args = VariantArray::new();
		args.push(button);
		args.push(action.into());
		args.push(event);
		button.assume_safe().set_toggle_mode(true);
		button.assume_safe()
			.connect(
				"toggled",
				owner.assume_shared(),
				"on_binding_button_toggled",
				args.into_shared(),
				1,
			)
			.unwrap_or_else(|e| {
				godot_error!("Failed to connect toggled signal for {}! {:#?}", text.new_ref(), e)
			});
		button
	}
}

trait Description {
	fn description(self) -> GodotString;
}

impl Description for Ref<InputEvent> {
	fn description(self) -> GodotString { unsafe {
		let event = match self.try_cast::<InputEventMouseButton>() {
			Ok(e) => return MOUSE_BUTTON_NAMES[e.assume_safe().button_index() as usize].into(),
			Err(e) => e,
		};
		
		let event = match event.try_cast::<InputEventKey>() {
			Ok(e) => return e.assume_safe().as_text(),
			Err(e) => e,
		};
		
		let event = match event.try_cast::<InputEventJoypadButton>() {
			Ok(e) => return CONTROLLER_BUTTON_NAMES[e.assume_safe().button_index() as usize].into(),
			Err(e) => e,
		};
		
		let event = match event.try_cast::<InputEventJoypadMotion>() {
			Ok(e) => {
				let e = e.assume_safe();
				let idx = ((e.axis() * 2) + (((e.axis_value().signum() as i64) + 1) / 2)) as usize;
				return AXIS_NAMES[idx].into();
			}
			Err(e) => e,
		};
		
		event.assume_safe().as_text()
	} }
}

/// Modified from the Godot engine editor settings input map editor source code
static CONTROLLER_BUTTON_NAMES: [&str; 16] = [
	"PS ✕\nXbox A\nNintendo B",
	"PS ◯\nXbox B\nNintendo A",
	"PS □\nXbox X\nNintendo Y",
	"PS △\nXbox Y\nNintendo X",
	"L (L1)",
	"R (R1)",
	"L2",
	"R2",
	"L3",
	"R3",
	"Select\nPS Share\nNintendo -",
	"Start\nPS Options\nNintendo +",
	"D-Pad ↑",
	"D-Pad ↓",
	"D-Pad ←",
	"D-Pad →",
];

/// Modified from the Godot engine editor settings input map editor source code
static AXIS_NAMES: [&str; 20] = [
	"Left Stick ←",
	"Left Stick →",
	"Left Stick ↑",
	"Left Stick ↓",
	"Right Stick ←",
	"Right Stick →",
	"Right Stick ↑",
	"Right Stick ↓",
	"Axis 4 -",
	"Axis 4 +",
	"Axis 5 -",
	"Axis 5 +",
	"Axis 6 -",
	"L2",
	"Axis 7 -",
	"R2",
	"Axis 8 -",
	"Axis 8 +",
	"Axis 9 -",
	"Axis 9 +",
];

/// Modified from the Godot engine editor settings input map editor source code
static MOUSE_BUTTON_NAMES: [&str; 10] = [
	"Button 0",
	"🖱 Left",
	"🖱 Right",
	"🖱 Middle",
	"Scroll up",
	"Scroll down",
	"Scroll left",
	"Scroll right",
	"Back",
	"Forward",
];
