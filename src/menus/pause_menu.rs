use gdnative::api::WindowDialog;
use gdnative::prelude::*;
use crate::scene_manager::SceneManager;
use gdnative::GodotResult;

#[derive(NativeClass, Default, Debug)]
#[inherit(WindowDialog)]
pub struct PauseMenu;

#[methods]
impl PauseMenu {
	fn new(_owner: &WindowDialog) -> Self {
		PauseMenu
	}
	
	#[export]
	fn quit(&self, owner: &WindowDialog) -> GodotResult {
		SceneManager::emit_signal(owner, "quit", &[])
	}
	
	#[export]
	fn on_continue_pressed(&self, owner: &WindowDialog) -> GodotResult {
		unsafe {
			GodotResult::from_variant(&owner.get_node("/root/SceneManager")?
				.assume_safe()
				.call_deferred("unpause", &[]))?
		}
	}
	
	#[export]
	fn on_tab_changed(&self, owner: &WindowDialog, _tab: u64) -> GodotResult {
		unsafe {
			GodotResult::from_variant(&owner.get_node("/root/SceneManager")?
				.assume_safe()
				.call_deferred("on_tab_changed", &[]))?
		}
	}
	
	#[export]
	fn _gui_input(&self, owner: &WindowDialog, event: Ref<InputEvent>) {
		if unsafe { event.assume_safe() }.is_action_pressed("ui_cancel", false) {
			owner.hide();
		}
	}
}
