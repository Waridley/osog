use gdnative::prelude::*;
use gdnative::api::{OS, CheckBox, Resource, ResourceSaver, VBoxContainer};
use crate::utils::GDExpect;
use gdnative::api::object::ConnectFlags;
use crate::save_data::GRAPHICS_SETTINGS_SAVE_PATH;
use crate::scene_manager::SceneManager;
use gdnative::GodotResult;

#[derive(NativeClass, Default, Debug)]
#[inherit(VBoxContainer)]
pub struct GraphicsMenu;

#[methods]
impl GraphicsMenu {
	
	fn new(_owner: &VBoxContainer) -> Self {
		GraphicsMenu
	}
	
	#[export]
	fn _ready(&self, owner: &VBoxContainer) {
		self.update_fullscreen_checkbox(owner);
		unsafe {
			owner
				.get_tree().gdexpect("Where's the tree??").assume_safe().root()
				.gdexpect("Where's the root viewport?!").assume_safe()
				.connect(
					"size_changed",
					owner.assume_shared(),
					"update_fullscreen_checkbox",
					VariantArray::new_shared(),
					ConnectFlags::DEFERRED.0,
				)
				.gdexpect("Couldn't connect size_changed signal!");
		}
	}
	
	#[export]
	fn update_fullscreen_checkbox(&self, owner: &VBoxContainer) {
		let ckbox = unsafe {
			owner
				.find_node("fullscreen_checkbox", true, true)
				.gdexpect("Couldn't find fullscreen_checkbox!").assume_safe()
				.cast::<CheckBox>().gdexpect("fullscreen_checkbox isn't a CheckBox!")
		};
		ckbox.set_block_signals(true);
		ckbox.set_pressed(OS::godot_singleton().is_window_fullscreen());
		ckbox.set_block_signals(false);
	}
	
	#[export]
	fn on_fullscreen_toggled(&self, owner: &VBoxContainer, value: bool) -> GodotResult {
		OS::godot_singleton().set_window_fullscreen(value);
		unsafe {
			// SceneManager::emit_signal(owner,"settings_changed", &[
			// 	Variant::from_object(owner.assume_shared()),
			// 	Variant::from_str("save"),
			// ])
			SceneManager::settings_changed(owner)
		}
	}
	
	#[export]
	fn save(&self, owner: &VBoxContainer) -> GodotResult {
		let save_path = GRAPHICS_SETTINGS_SAVE_PATH;
		
		let script = ResourceLoader::godot_singleton()
			.load("res://scripts/GraphicsSettings.gdns", "NativeScript", false)
			.gdexpect("Couldn't find GraphicsSettings script!");
		
		let res = Resource::new();
		res.set_script(script);
		
		ResourceSaver::godot_singleton()
			.save(save_path, res, 0)
			.gdexpect(&*format!("Couldn't save graphics settings to {}!", save_path));
		SceneManager::emit_signal(owner, "settings_saved", &[])
	}
	
}