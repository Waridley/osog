use gdnative::api::{AudioStreamGeneratorPlayback, AudioStreamPlayer, AudioStreamGenerator};
use gdnative::prelude::*;

use crate::utils::*;
use interpolation::Lerp;

#[derive(Default, Debug)]
struct Wave {
	frequency: f64,
	amplitude: f32,
	index: usize,
	samples: Vec<f32>,
}

impl Wave {
	fn new(frequency: f64, sample_rate: f64) -> Self {
		let num_samples = (sample_rate / frequency).ceil() as usize;
		let mut samples = Vec::with_capacity(num_samples);
		
		for i in 0..num_samples {
			samples.push(
				0.0
					.lerp(&(PI * 2.0), &((i as f64) / (num_samples as f64)))
					.sin() as f32,
			);
		}
		
		Wave {
			frequency,
			amplitude: 1.0,
			index: 0,
			samples,
		}
	}
	
	pub fn value(&self) -> f32 {
		self.samples[self.index] * (self.amplitude as f32)
	}
}

#[derive(NativeClass, Default, Debug)]
#[user_data(RwLockData<Radar>)]
#[inherit(AudioStreamPlayer)]
pub(crate) struct Radar {
	phase: f64,
	#[property(default = 55.0)] base_frequency: f64,
	waves: [Wave; 5],
	pub closest_target_y_pos: f32,
	playback: Option<Ref<AudioStreamGeneratorPlayback>>,
}

#[methods]
impl Radar {
	fn new(_owner: &AudioStreamPlayer) -> Self {
		Radar {
			base_frequency: 55.0,
			..Default::default()
		}
	}
	
	#[export]
	fn _ready(&mut self, owner: &AudioStreamPlayer) {
		self.playback = owner.get_stream_playback()
			.gdexpect("Couldn't find stream_playback")
			.cast::<AudioStreamGeneratorPlayback>();
		let rate = unsafe {
			owner
				.stream().gdexpect("Couldn't find stream!")
				.cast::<AudioStreamGenerator>().gdexpect("Stream isn't an AudioStreamGenerator!")
				.assume_safe()
				.mix_rate()
		};
		let freq = self.base_frequency;
		self.waves = [
			Wave::new(freq, rate),
			Wave::new(freq * 3.0, rate),
			Wave::new(freq * 5.0, rate),
			Wave::new(freq * 8.0, rate),
			Wave::new(freq * 10.0, rate),
		];
		self.fill_buffer(owner);
	}
	
	#[export]
	fn _process(&mut self, owner: &AudioStreamPlayer, _delta: f64) {
		self.fill_buffer(owner)
	}
	
	fn fill_buffer(&mut self, _owner: &AudioStreamPlayer) {
		unsafe {
			let playback = self.playback.as_ref()
				.gdexpect("stream_playback isn't an AudioStreamGeneratorPlayback!").clone();
			let to_fill = playback.assume_safe().get_frames_available();
			for _ in 0..to_fill {
				self.advance();
				self.compute_vertical_feedback();
				let sample = self.mix();
				//TODO pan left/right here instead of using panner effect
				//    *** except that might delay the response due to the AudioStreamGenerator buffer
				
				// let pan = -((((self.closest_target_h_angle + (TAU * 0.15) as f32) / (TAU * 0.3) as f32)
				// 		.calc(EaseFunction::QuinticInOut) * 2.0) - 1.0);
				let frame = Vector2::new(sample, sample);
				
				playback.assume_safe().push_frame(frame);
			}
		}
	}
	
	fn advance(&mut self) {
		for wave in &mut self.waves {
			wave.index = (wave.index + 1) % wave.samples.len()
		}
	}
	
	fn compute_vertical_feedback(&mut self) {
		let target_amplitudes = [
			f32::max(1.0 - f32::max((self.closest_target_y_pos * 6.0).powi(3), 0.0), 0.0, ),
			f32::max(1.0 - f32::max((self.closest_target_y_pos * 6.0).powi(3), 0.0), 0.0, ),
			f32::max(1.0 -       (self.closest_target_y_pos.abs() * 10.0).powi(5), 0.0),
			f32::max(1.0 + f32::min((self.closest_target_y_pos * 5.0).powi(3), 0.0), 0.0, ),
			f32::max(1.0 + f32::min((self.closest_target_y_pos * 5.0).powi(3), 0.0), 0.0, ),
		];
		for (i, wave) in self.waves.iter_mut().enumerate() {
			wave.amplitude = wave.amplitude.lerp(&target_amplitudes[i], &0.1f32);
		}
	}
	
	fn mix(&self) -> f32 {
		let mut sum = 0.0;
		for wave in &self.waves {
			sum += wave.value() * wave.amplitude
		}
		sum / 5.0
	}
}
