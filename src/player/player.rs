use crate::utils::*;
use gdnative::api::{
	input::MouseMode, AudioEffectAmplify, AudioEffectPanner, AudioServer, AudioStream,
	AudioStreamPlayer, Camera, File, Input, InputEvent, InputEventMouseMotion, KinematicBody, Node,
	RayCast, Resource, ShaderMaterial, Spatial, Sprite,
};
use gdnative::core_types::{Angle, Variant, Vector2, Vector2Godot, Vector3};
use gdnative::prelude::*;
use interpolation::{Ease, EaseFunction, Lerp};

use crate::player::Radar;
use crate::targets::TargetManager;
use rand::prelude::ThreadRng;
use rand::Rng;
use gdnative::GodotResult;

const UP_VEC: Vector3 = Vector3::new(0.0, 1.0, 0.0);
const FORWARD_VEC: Vector3 = Vector3::new(0.0, 0.0, -1.0);

#[rustfmt::skip]
#[derive(NativeClass, Default, Debug)]
#[inherit(Resource)]
pub(crate) struct PlayerSettings {
	#[property(default = 0.100)] pub sensitivity: f64,
	#[property(default = 0.380)] pub scoped_sens_ratio: f64,
}

#[methods]
impl PlayerSettings {
	pub(crate) fn new(_owner: &Resource) -> Self {
		PlayerSettings {
			sensitivity: 0.1,
			scoped_sens_ratio: 0.38,
			..Default::default()
		}
	}
}

#[rustfmt::skip] // I spent too much time getting the properties to look pretty >:(
#[derive(NativeClass, Default, Debug)]
#[inherit(KinematicBody)]
pub(crate) struct Player {
	//region editor_properties
	#[property(default = 100.00)] fov: f64,
	#[property(default = 60.000)] scoped_fov: f64,
	#[property(default =  5.000)] max_speed: f64,
	#[property(default =  0.400)] scoped_speed_ratio: f64,
	#[property(default =  1.560)] max_look_radians: f64,
	#[property(default =  4.000)] acceleration: f64,
	#[property(default = 98.100)] gravity: f64,
	#[property(default = 20.000)] jump_velocity: f64,
	#[property(default =  1.000)] fire_cooldown: f64,
	#[property] pub settings: Option<Instance<PlayerSettings, Shared>>,
	//endregion editor_properties
	
	pub sensitivity: f64,
	pub scoped_sens_ratio: f64,
	
	//region node_refs
	head: Option<Ref<Spatial>>,
	radar_asp: Option<Ref<AudioStreamPlayer>>,
	radar: Option<RefInstance<'static, Radar, Shared>>,
	hitscan_ray: Option<Ref<RayCast>>,
	target_mgr: Option<RefInstance<'static, TargetManager, Shared>>,
	cam: Option<Ref<Camera>>,
	vignette: Option<Ref<ShaderMaterial>>,
	//endregion node_refs
	
	//region internal_vars
	zoom: f64,
	speed_cap: f64,
	vel: Vector3,
	rem_fire_cd: f64,
	//endregion internal_vars
}

#[methods]
impl Player {
	fn new(_owner: &KinematicBody) -> Self {
		Player {
			fov: 100.0,
			scoped_fov: 60.0,
			max_speed: 5.0,
			scoped_speed_ratio: 0.4,
			max_look_radians: 1.57,
			acceleration: 4.0,
			gravity: 98.1,
			jump_velocity: 20.0,
			fire_cooldown: 1.0,
			sensitivity: 0.1,
			scoped_sens_ratio: 0.38,
			..Default::default()
		}
	}
	
	#[export]
	fn _ready(&mut self, owner: &KinematicBody) -> Option<()> {
		//region declarations
		let input = Input::godot_singleton();
		
		let Player {
			head,
			radar_asp,
			radar,
			hitscan_ray,
			target_mgr,
			cam,
			vignette,
			settings,
			..
		} = self;
		//endregion declarations
		
		input.set_mouse_mode(MouseMode::CAPTURED.0);
		input.set_use_accumulated_input(false);
		
		unsafe {
			*head = Some(
				owner
					.get_node("Head")
					.gdexpect("Couldn't find Head node")
					.assume_safe()
					.cast::<Spatial>()?
					.assume_shared(),
			);
			*radar_asp = Some(
				((*head)?.assume_safe())
					.get_node("Radar")
					.gdexpect("Couldn't find Radar node")
					.assume_safe()
					.cast::<AudioStreamPlayer>()?
					.assume_shared(),
			);
			let asp = (*radar_asp)?.assume_safe();
			asp.set_volume_db(-80.0);
			asp.play(0.0);
			*radar = asp.cast_instance::<Radar>();
			let target_mgr_node = owner
				.get_tree()?
				.assume_safe()
				.root()?
				.assume_safe()
				.get_node("TargetManager")
				.gdexpect("Couldn't find TargetManager node");
			*target_mgr = target_mgr_node.assume_safe().cast_instance();
			*cam = Some(
				head
					.gdexpect("Couldn't find \"Head\" node")
					.assume_safe()
					.get_node("Camera")
					.gdexpect("Couldn't find \"Camera\" node")
					.assume_safe()
					.cast::<Camera>()?
					.assume_shared(),
			);
			*hitscan_ray = Some(
				(*cam)?
					.assume_safe()
					.get_node("HitscanRay")
					.gdexpect("Couldn't find HitscanRay node")
					.assume_safe()
					.cast::<RayCast>()?
					.assume_shared(),
			);
			*vignette = Some(
				(*cam)?
					.assume_safe()
					.get_node("Vignette")
					.gdexpect("Couldn't find Vignette node")
					.assume_safe()
					.cast::<Sprite>()
					.gdexpect("Vignette node isn't an instance of Sprite")
					.material()
					.gdexpect("Couldn't get the Vignette Sprite's material")
					.cast::<ShaderMaterial>()?,
			);
			let saved_settings_file_path = "res://test_saves/saved_player_settings.tres";
			*settings = match File::new().file_exists(saved_settings_file_path) {
				true => ResourceLoader::godot_singleton()
					.load(saved_settings_file_path, "Resource", false)?
					.cast_instance(),
				false => ResourceLoader::godot_singleton()
					.load("res://default_player_settings.tres", "Resource", false)?
					.cast_instance(),
			};
			
			owner.set_process_input(true);
		}
		Some(())
	}
	
	#[export]
	fn _process(&mut self, owner: &KinematicBody, delta: f64) -> GodotResult {
		let input = Input::godot_singleton();
		
		self.controller_aim_input(input, owner, delta)?;
		
		if let Player {
			zoom,
			speed_cap,
			max_speed,
			scoped_speed_ratio,
			fov,
			scoped_fov,
			rem_fire_cd,
			cam: Some(cam),
			radar_asp: Some(asp),
			vignette: Some(vignette),
			..
		} = self
		{
			if input.is_action_pressed("scope_in") {
				*zoom = f64::min(*zoom + delta * 1.25, 1.0);
				*speed_cap = *max_speed * *scoped_speed_ratio;
			} else {
				*zoom = f64::max(*zoom - delta * 1.25, 0.0);
				*speed_cap = *max_speed;
			}
			
			let asrv = AudioServer::godot_singleton();
			let dbus_idx = asrv.get_bus_index("DingerBus");
			
			unsafe {
				asrv
					.get_bus_effect(dbus_idx, 0)?
					.assume_safe()
					.cast::<AudioEffectAmplify>()?
					.set_volume_db((-18.0f64).lerp(&0.0f64, &(1.0 - *zoom)));
				asp.assume_safe().set_volume_db((-80.0f64).lerp(&(-12.0), zoom));
				cam.assume_safe().set_fov(scoped_fov.lerp(fov, &(1.0 - *zoom)));
				vignette
					.assume_safe()
					.set_shader_param("amount", Variant::from_f64(*zoom * 0.8));
			}
			*rem_fire_cd -= delta;
		} else {
			godot_error!("Couldn't bind some nodes ⋮ {:#?}", self)
		}
		Ok(())
	}
	
	#[export]
	fn _physics_process(&mut self, owner: &KinematicBody, delta: f64) {
		let input = Input::godot_singleton();
		let vel = self.movement_input(input, owner, delta);
		self.physics_movement(vel, owner, delta);
	}
	
	#[export]
	fn _unhandled_input(&mut self, owner: &KinematicBody, event: Ref<InputEvent>) -> GodotResult {
		let input = Input::godot_singleton();
		
		if let Player {
			head: Some(head),
			hitscan_ray,
			max_look_radians: max_look,
			sensitivity,
			scoped_sens_ratio,
			zoom,
			fire_cooldown,
			rem_fire_cd,
			..
		} = self
		{
			unsafe {
				let sens = *sensitivity * 0.01;
				let sens = sens.lerp(
					&(sens * *scoped_sens_ratio),
					&(*zoom).calc(EaseFunction::CubicOut),
				);
				let head = head.assume_safe();
				let event = event.assume_safe();
				
				if let Some(e) = event.cast::<InputEventMouseMotion>() {
					if input.get_mouse_mode() != MouseMode::CAPTURED {
						return Ok(());
					}
					
					let head_rot = head.rotation();
					let new_head_rot_x = match head_rot.x as f64 + (-sens * e.relative().y as f64) {
						x if x > *max_look => *max_look,
						x if x < -*max_look => -*max_look,
						x => x,
					};
					let h_angle = -sens * e.relative().x as f64;
					owner.rotate_y(h_angle);
					head.set_rotation(Vector3::new(new_head_rot_x as f32, head_rot.y, head_rot.z));
					
					self.update_radar(owner)
				} else if event.is_action_pressed("fire", false) && *rem_fire_cd <= 0.0 {
					*rem_fire_cd = *fire_cooldown;
					
					let result = hitscan_ray.unwrap().assume_safe().get_collider();
					let asp = owner
						.get_node("FireSoundPlayer")?
						.assume_safe()
						.cast::<AudioStreamPlayer>()?;
					let path = match result {
						Some(thing) => {
							let name = thing
								.assume_safe()
								.cast::<Node>()?
								.get_parent()?
								.assume_safe()
								.name()
								.to_string();
							godot_print!("Hit {}", name);
							move_target(
								&*thing
									.assume_safe()
									.cast::<Node>()?
									.get_parent()?
									.assume_safe()
									.cast::<Spatial>()?,
							);
							if *zoom < 0.9 {
								"res://sounds/ChargeShotLong_Weak.wav"
							} else {
								"res://sounds/ShotExplosionDirect00.wav"
							}
						}
						None => {
							godot_print!("Miss!");
							if *zoom < 0.9 {
								"res://sounds/ShotExplosionLight00.wav"
							} else {
								"res://sounds/ShotExplosion00.wav"
							}
						}
					};
					let stream = ResourceLoader::godot_singleton()
						.load(path, "AudioStream", false)?
						.cast::<AudioStream>()?;
					asp.set_stream(stream);
					asp.play(0f64);
					
					*zoom = 0.0; //TODO: Animate unscoping instead of snapping
					Ok(())
				} else {
					Ok(())
				}
			}
		} else {
			godot_error!("Couldn't bind some fields: {:#?}", self);
			Err(GodotError::PrinterOnFire)
		}
	}
	
	fn update_radar(&self, owner: &KinematicBody) -> GodotResult {
		let audio_server = AudioServer::godot_singleton();
		
		if let Player {
			head: Some(head),
			radar_asp: Some(asp),
			radar,
			target_mgr,
			..
		} = self {
			unsafe {
				let radar_bus_name = asp.assume_safe().bus();
				let radar_bus_idx = audio_server.get_bus_index(radar_bus_name);
				let pan_effect = audio_server
					.get_bus_effect(radar_bus_idx, 0)
					.gdexpect("No effects in RadarPan bus")
					.cast::<AudioEffectPanner>()
					.gdexpect("First effect in the bus isn't a Panner");
				let amplify_effect = audio_server
					.get_bus_effect(radar_bus_idx, 1)
					.gdexpect("Couldn't find Amplify effect")
					.cast::<AudioEffectAmplify>()
					.gdexpect("Second effect in the bus isn't an Amplify");
				
				let mut closest_target_projected_distance = f32::MAX;
				let mut closest_target_rel_x_pos = f32::MAX;
				let mut closest_target_rel_y_pos = f32::MAX;
				target_mgr
					.as_ref()?
					.map(|script, _tm_owner| {
						for target in &script.targets {
							let target_direction_rel_to_body = owner
								.to_local(target.assume_safe().global_transform().origin)
								.normalize();
							
							let target_x_pos = target_direction_rel_to_body
								.xz()
								.angle_to(FORWARD_VEC.xz())
								.get();
							let target_y_pos = Vector2::new(1.0, 0.0)
								.angle_to(Vector2::new(
									target_direction_rel_to_body.xz().length(),
									target_direction_rel_to_body.y,
								))
								.get()
								.tan();
							
							let aim_point_y = head.assume_safe().rotation().x.tan(); //rotation around the x axis
							
							let projected_distance =
									Vector2::new(target_x_pos, target_y_pos - aim_point_y).length();
							
							if projected_distance < closest_target_projected_distance {
								closest_target_projected_distance = projected_distance;
								
								closest_target_rel_x_pos = target_x_pos;
								closest_target_rel_y_pos = target_y_pos - aim_point_y;
							}
						}
					})
					.gdexpect("Failed to map Instance of TargetManager");
				
				amplify_effect.assume_safe().set_volume_db(0.0.lerp(
					&(-80.0f64),
					&((closest_target_projected_distance * 0.5).calc(EaseFunction::QuinticIn)
							as f64),
				));
				let pan = -((((closest_target_rel_x_pos as f64 + (TAU * 0.15)) / (TAU * 0.3))
					.calc(EaseFunction::QuinticInOut)
					* 2.0)
					- 1.0) as f64;
				pan_effect.assume_safe().set_pan(pan);
				
				radar
					.as_ref()?
					.map_mut(|script, _owner| {
						script.closest_target_y_pos = closest_target_rel_y_pos;
					})
					.gdexpect("Failed to map Instance of Radar")
			}
		}
		Ok(())
	}
	
	fn movement_input(&self, input: &Input, owner: &KinematicBody, delta: f64) -> Vector3 {
		let accel = self.acceleration;
		
		let lr = input.get_action_strength("move_right") - input.get_action_strength("move_left");
		let fb = input.get_action_strength("move_back") - input.get_action_strength("move_forward");
		
		//Cap diagonal speed
		let lr2 = lr * lr;
		let fb2 = fb * fb;
		let len2 = lr2 + fb2;
		let new_len = f64::min(len2.sqrt(), 1.0) * self.speed_cap;
		let new_len2 = new_len * new_len;
		let new_lr2 = if lr.abs() < f32::EPSILON as f64 {
			0.0
		} else {
			(lr2 * new_len2) / len2
		};
		let new_fb2 = if fb.abs() < f32::EPSILON as f64 {
			0.0
		} else {
			(fb2 * new_len2) / len2
		};
		
		let lr = (new_lr2.sqrt() * lr.signum()) as f32;
		let fb = (new_fb2.sqrt() * fb.signum()) as f32;
		
		let target_h_vel = Vector2::new(self.vel.x, self.vel.z)
			.lerp(Vector2::new(lr, fb), f32::max((accel * delta) as f32, 1.0))
			.rotated(Angle {
				radians: -owner.rotation().y,
			});
		
		let y = if input.is_action_just_pressed("jump") && owner.is_on_floor() {
			self.jump_velocity
		} else {
			self.vel.y as f64
		};
		
		Vector3::new(target_h_vel.x, y as f32, target_h_vel.y)
	}
	
	/// Mouse aim is done in `_input` so it can respond immediately to every motion event.
	/// Controller sticks only send events when they are moved, but aim should continuously
	/// be adjusted if a direction is held. Remembering the last event would risk dropping
	/// events and getting stuck turning in one direction.
	fn controller_aim_input(&self, input: &Input, owner: &KinematicBody, _delta: f64) -> GodotResult {
		let h_stick_pos =
			input.get_action_strength("look_right") - input.get_action_strength("look_left");
		let v_stick_pos =
			input.get_action_strength("look_up") - input.get_action_strength("look_down");
		unsafe {
			let head = self
				.head
				.gdexpect("Player doesn't have a head!")
				.assume_safe();
			let sens = self.sensitivity as f64 * 0.5f64;
			let sens = sens.lerp(
				&(sens * self.scoped_sens_ratio),
				&(self.zoom).calc(EaseFunction::CubicOut),
			);
			
			let head_rot = head.rotation();
			let new_head_rot_x = match head_rot.x as f64 + (-sens * v_stick_pos) {
				x if x > self.max_look_radians => self.max_look_radians,
				x if x < -self.max_look_radians => -self.max_look_radians,
				x => x,
			};
			let h_angle = (-sens * h_stick_pos) as f64;
			owner.rotate_y(h_angle);
			head.set_rotation(Vector3::new(new_head_rot_x as f32, head_rot.y, head_rot.z));
		}
		self.update_radar(owner)
	}
	
	fn physics_movement(&mut self, vel: Vector3, owner: &KinematicBody, delta: f64) {
		self.vel = owner.move_and_slide(vel, UP_VEC, false, 4, PI * 0.125, true);
		self.vel.y -= (self.gravity * delta) as f32;
	}
}

fn move_target(target: &Spatial) {
	let mut rng = ThreadRng::default();
	target.set_translation(Vector3::new(
		rng.gen_range(-25.0, 25.0),
		rng.gen::<f32>().calc(EaseFunction::CubicIn) * 10.0,
		rng.gen_range(-25.0, 25.0),
	));
}
