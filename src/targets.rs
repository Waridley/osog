use crate::utils::GDExpect;
use gdnative::api::{AudioStreamPlayer3D, Node, Spatial,};
use gdnative::prelude::*;
use rand::prelude::*;
use gdnative::GodotResult;

#[derive(NativeClass, Default, Debug)]
#[inherit(Node)]
pub(crate) struct TargetManager {
	#[property(default = 2.0f64)] min_time: f64,
	#[property(default = 5.0f64)] max_time: f64,
	pub(crate) targets: Vec<Ref<Spatial>>,
	time_till_play: f64,
	rng: ThreadRng,
}

#[methods]
impl TargetManager {
	fn new(_owner: &Node) -> Self {
		TargetManager {
			min_time: 2.0f64,
			max_time: 5.0f64,
			time_till_play: 2.0,
			..Default::default()
		}
	}
	
	#[export]
	fn _ready(&self, owner: &Node) {
		owner.set_pause_mode(Node::PAUSE_MODE_PROCESS);
		
	}
	
	#[export]
	fn register(&mut self, _owner: &Node, target: Ref<Spatial>) {
		self._register(target)
	}
	
	fn _register(&mut self, target: Ref<Spatial>) {
		unsafe {
			let target = target.assume_safe();
			godot_print!("Registering {} at {:?}", target.name(), target.global_transform().origin);
		}
		self.targets.push(target);
	}
	
	#[export]
	fn _process(&mut self, _owner: &Node, delta: f64) {
		self.time_till_play -= delta;
		if self.time_till_play <= 0.0f64 {
			self.play_random_ding();
			self.time_till_play = self.rng.gen_range(self.min_time, self.max_time);
		}
	}
	
	fn play_random_ding(&mut self) {
		let i: usize = self.rng.gen_range(0, self.targets.len());
		
		let dinger = unsafe {
			self.targets[i].assume_safe().get_node("Dinger")
				.gdexpect("Couldn't find \"Dinger\" node")
				.assume_safe()
				.cast::<AudioStreamPlayer3D>()
				.gdexpect("\"Dinger\" is not an instance of AudioStreamPlayer3D")
		};
		dinger.play(0.0f64);
	}
}

#[derive(NativeClass, Default, Debug)]
#[inherit(Spatial)]
pub(crate) struct Target;

#[methods]
impl Target {
	fn new(_owner: &Spatial) -> Self {
		Target
	}
	
	#[export]
	fn _ready(&self, owner: &Spatial) -> GodotResult {
		unsafe {
			owner.get_node("/root/TargetManager")
				.gdexpect("Couldn't find TargetManager").assume_safe()
				.cast_instance::<TargetManager>().gdexpect("Couldn't cast TargetManager")
				.map_mut(|script, _| script._register(owner.assume_shared()))
				.map_err(|_| GodotError::ScriptFailed)
		}
	}
}
