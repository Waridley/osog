use crate::utils::GDExpect;
use gdnative::api::{
	input::MouseMode, PackedScene, WindowDialog, OS, ConfirmationDialog, AcceptDialog,
	object::ConnectFlags
};
use gdnative::prelude::*;
use crate::save_data::{GRAPHICS_SETTINGS_SAVE_PATH,};
use gdnative::GodotResult;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::fmt;

const PAUSE_MENU_SCENE_PATH: &str = "res://menus/PauseMenu.tscn";

#[derive(NativeClass, Default, Debug)]
#[inherit(Node)]
#[register_with(Self::register_signals)]
pub struct SceneManager {
	pause_popup_scene: Option<Ref<PackedScene>>,
	pause_popup: Option<Ref<WindowDialog>>,
	canvas_layer: Option<Ref<CanvasLayer>>,
	tree: Option<Ref<SceneTree>>,
	unsaved_settings: Vec<Ref<Node>>,
}

// static mut SINGLETON: Option<RefInstance<SceneManager, Shared>> = None;

#[methods]
impl SceneManager {
	
	fn new(owner: &Node) -> Self {
		Self::default()
	}
	
	fn register_signals(builder: &ClassBuilder<Self>) {
		builder.add_signal(Signal {
			name: "settings_changed",
			args: &[
				SignalArgument {
					name: "menu_node",
					default: Variant::new(),
					export_info: ExportInfo::new(VariantType::Object),
					usage: PropertyUsage::NO_INSTANCE_STATE,
				},
				SignalArgument {
					name: "method",
					default: Variant::from_str("save"),
					export_info: ExportInfo::new(VariantType::GodotString),
					usage: PropertyUsage::NO_INSTANCE_STATE,
				}
			],
		});
		builder.add_signal(Signal {
			name: "settings_saved",
			args: &[],
		});
		builder.add_signal(Signal {
			name: "quit",
			args: &[],
		});
	}
	
	#[export]
	fn _enter_tree(&self, owner: &Node) -> GodotResult {
		// Setters will be called when the resource is loaded
		// The setter for `GraphicsSettings::fullscreen` will call `OS::set_window_fullscreen()` directly
		ResourceLoader::godot_singleton()
			.load(GRAPHICS_SETTINGS_SAVE_PATH, "Resource", false)
			.gdexpect("Couldn't load graphics settings!");
		Ok(())
	}
	
	#[export]
	fn _ready(&mut self, owner: &Node) -> GodotResult {
		owner.set_pause_mode(Node::PAUSE_MODE_PROCESS);
		owner.set_process_input(true);
		self.pause_popup_scene = Some(
			ResourceLoader::godot_singleton()
				.load(PAUSE_MENU_SCENE_PATH, "PackedScene", false)?
				.cast::<PackedScene>()?,
		);
		unsafe {
			self.canvas_layer = Some(CanvasLayer::new().assume_shared());
			self.tree = owner.get_tree();
			owner.connect(
				"settings_changed",
				owner.assume_shared(),
				"on_settings_changed",
				VariantArray::new_shared(),
				0,
			).gdexpect("Failed to connect settings_changed signal!");
			owner.connect(
				"settings_saved",
				owner.assume_shared(),
				"on_settings_saved",
				VariantArray::new_shared(),
				0,
			).gdexpect("Failed to connect settings_saved signal!");
			owner.connect(
				"quit",
				owner.assume_shared(),
				"on_quit_requested",
				VariantArray::new_shared(),
				0,
			).gdexpect("Failed to connect quit signal!");
		}
		owner.add_child(self.canvas_layer.as_ref()?, true);
		
		unsafe {
			GodotResult::from_variant(&owner.call_deferred("pause", &[]))?
		}
	}
	
	#[export]
	fn _input(&mut self, owner: &Node, event: Ref<InputEvent>) -> GodotResult {
		let event = unsafe { event.assume_safe() };
		
		if event.is_action_pressed("pause", false) {
			let tree = unsafe { self.tree?.assume_safe() };
			tree.set_input_as_handled();
			if !tree.is_paused() {
				self.pause(owner)
			} else {
				self.unpause(owner)
			}
		} else if event.is_action_pressed("fullscreen", false) {
			let os = OS::godot_singleton();
			os.set_window_fullscreen(!os.is_window_fullscreen());
			Ok(())
		} else {
			Ok(())
		}
	}
	
	#[export]
	fn pause(&mut self, owner: &Node) -> GodotResult {
		Input::godot_singleton().set_mouse_mode(MouseMode::VISIBLE.0);
		if self.pause_popup.is_none() {
			unsafe {
				self.pause_popup = Some(
					self.pause_popup_scene
                        .as_ref().gdexpect("Couldn't load Pause_Popup scene!").assume_safe()
                        .instance(PackedScene::GEN_EDIT_STATE_DISABLED)
                        .gdexpect("Couldn't instance Pause_Popup scene!")
						.assume_safe()
                        .cast::<WindowDialog>().gdexpect("PausePopup isn't a WindowDialog!")
						.claim()
				);
				let window_dialog = self.pause_popup
					.gdexpect("Couldn't instance popup!").assume_safe()
					.cast::<WindowDialog>().gdexpect("Pause_Popup is not a WindowDialog!");
				window_dialog.set_exclusive(true);
				self.canvas_layer?.assume_safe().add_child(window_dialog, false);
				window_dialog.popup_centered(Vector2::new(0.0, 0.0));
				window_dialog.connect(
					"popup_hide",
					owner.assume_shared(),
					"unpause",
					VariantArray::new_shared(),
					0,
				).gdexpect("Failed to connect popup_hide signal to unpause!");
			}
		} else {
			godot_warn!("Pausing while popup still exists!")
		}
		unsafe { self.tree?.assume_safe().set_pause(true) }
		Ok(())
	}
	
	#[export]
	fn unpause(&mut self, owner: &Node) -> GodotResult {
		match unsafe { self.leaving_settings_screen(owner, "on_unpause_confirmation_hide") } {
			Ok(()) => unsafe {
				if let Some(popup) = self.pause_popup {
					popup.assume_safe().disconnect("popup_hide", owner.assume_shared(), "unpause");
					popup.assume_safe().queue_free();
					self.pause_popup = None;
					self.tree?.assume_safe().set_pause(false);
					Input::godot_singleton().set_mouse_mode(MouseMode::CAPTURED.0)
				} else {
					godot_warn!("Unpausing while popup is already None!");
					self.tree?.assume_safe().set_pause(false);
				}
				Ok(())
			},
			Err(e) => {
				println!("{}", e);
				Err(GodotError::Busy)
			}
		}
	}
	
	unsafe fn leaving_settings_screen(&self, owner: &Node, method_to_call_after: impl Into<GodotString>) -> Result<(), UnsavedError> {
		if let Some(menu_node) = self.unsaved_settings.first() {
			let dialog = ConfirmationDialog::new();
			dialog.set_text("Do you want to save your changed settings?");
			dialog.set_exclusive(true);
			let ok = dialog.get_ok().unwrap().assume_safe();
			ok.set_text("✔ Save");
			let cancel = dialog.get_cancel().unwrap().assume_safe();
			cancel.set_text("✘ Don't save");
			let dialog = dialog.into_shared();
			self.canvas_layer.unwrap().assume_safe().add_child(dialog, false);
			dialog.assume_safe().popup_centered(Vector2::new(0.0, 0.0));
			dialog.assume_safe().connect("confirmed", owner.assume_shared(), "save_settings", VariantArray::new_shared(), 0)
			      .gdexpect("Failed to connect ok signal!");
			let args = VariantArray::new();
			args.push(Variant::from_object(dialog));
			dialog.assume_safe()
			      .connect(
				      "popup_hide",
				      owner.assume_shared(),
				      method_to_call_after,
				      args.into_shared(),
				      ConnectFlags::DEFERRED.0,
			      )
			      .gdexpect("Failed to connect popup_hide signal!");
			Err(UnsavedError { unsaved_menus: self.unsaved_settings.clone() })
		} else {
			Ok(())
		}
	}
	
	#[export]
	fn on_tab_changed(&mut self, owner: &Node) -> GodotResult {
		// unsafe { self.leaving_settings_screen(owner, "on_tab_changed_confirmation_hide") }
		// 	.map_err(|e| {
		// 		println!("{}", e);
		// 		GodotError::Busy
		// 	})
		Ok(())
	}
	
	#[export]
	fn on_tab_changed_confirmation_hide(&mut self, _owner: &Node, dialog: Ref<ConfirmationDialog>) {
		// self.changed_menu_save_method = None;
		unsafe { dialog.assume_safe() }.queue_free()
	}
	
	#[export]
	fn on_unpause_confirmation_hide(&mut self, owner: &Node, dialog: Ref<ConfirmationDialog>) -> GodotResult {
		self.unsaved_settings.clear();
		unsafe { dialog.assume_safe() }.queue_free();
		self.unpause(owner)
	}
	
	#[export]
	fn on_settings_changed(&mut self, _owner: &Node, menu_node: Ref<Node>) {
		self.unsaved_settings.push((menu_node));
		godot_print!("Settings changed")
	}
	
	#[export]
	fn on_settings_saved(&mut self, _owner: &Node) {
		// self.changed_menu_save_method = None;
		godot_print!("Settings saved")
	}
	
	#[export]
	fn on_quit_requested(&mut self, owner: &Node) -> GodotResult {
		if let Some(popup) = self.pause_popup { unsafe {
			popup.assume_safe()
				.connect(
					"popup_hide",
					owner.assume_shared(),
					"quit",
					VariantArray::new_shared(),
					0
				)
				.gdexpect("Couldn't connect popup_hide to quit!");
			self.unpause(owner)
		} } else {
			self.quit(owner)
		}
	}
	
	#[export]
	fn quit(&self, owner: &Node) -> GodotResult {
		unsafe { owner.get_tree()?.assume_safe().quit(0) }
		Ok(())
	}
	
	pub(crate) fn emit_signal(owner: &Node, signal: impl Into<GodotString>, varargs: &[Variant])
		-> GodotResult {
		unsafe {
			owner
				.get_node("/root/SceneManager")?
				.assume_safe()
				.emit_signal(signal, varargs);
		}
		Ok(())
	}
	
	pub(crate) fn warn(owner: &Node, message: impl Into<GodotString>) -> GodotResult {
		let acc_dialog = AcceptDialog::new();
		acc_dialog.set_text(message);
		let canvas_layer = unsafe { owner.get_node("/root/SceneManager/CanvasLayer")?.assume_safe() };
		let acc_dialog = acc_dialog.into_shared();
		canvas_layer.add_child(acc_dialog, false);
		unsafe { acc_dialog.assume_safe().popup_centered(Vector2::new(0.0, 0.0)) }
		Ok(())
	}
	
	pub(crate) fn settings_changed(menu: &Node) -> GodotResult {
		godot_print!("{} settings changed", menu.name());
		let scene_manager = unsafe { menu.get_node("/root/SceneManager")?.assume_safe() };
		scene_manager.cast_instance::<SceneManager>()?
			.map_mut(|this, owner| {
				let mut found = false;
				for node in &this.unsaved_settings {
					if unsafe { node.assume_safe() }.get_path() == menu.get_path() {
						found = true;
						break
					}
				}
				if !found {
					unsafe { this.unsaved_settings.push(menu.assume_shared()); }
				}
			})
			.gdexpect("Couldn't map SceneManager instance!");
		Ok(())
	}
	
	pub(crate) fn settings_saved(menu: &Node) -> GodotResult {
		let scene_manager = unsafe { menu.get_node("/root/SceneManager")?.assume_safe() };
		
			scene_manager.cast_instance::<SceneManager>()?
				.map_mut(|this, owner| {
					this.unsaved_settings
					    .retain(|node| {
						    unsafe { node.assume_safe().get_path() == menu.get_path() }
					    })
				})
				.gdexpect("Couldn't map SceneManager instance!");
		godot_print!("{} settsing saved", menu.name());
		Ok(())
	}
	
	#[export]
	fn save_settings(&mut self, _owner: &Node) {
		for node in self.unsaved_settings.drain(..) {
			unsafe { node.assume_safe().call_deferred("save", &[]) };
		}
	}
	
}

#[derive(Debug)]
struct UnsavedError {
	unsaved_menus: Vec<Ref<Node>>,
}
impl Error for UnsavedError {}

impl Display for UnsavedError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "Settings not saved in {}", unsafe {
			self.unsaved_menus.iter()
				.map(|node| node.assume_safe().name().to_string())
				.collect::<Vec<String>>()
				.join(", ")
			
		})
	}
}