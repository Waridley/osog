use crate::player::PlayerSettings;
use gdnative::api::{Resource, OS,};
use gdnative::prelude::*;

#[derive(NativeClass, Default)]
#[inherit(Resource)]
pub(crate) struct Settings {
	#[property]
	player: Option<Instance<PlayerSettings, Shared>>,
	graphics: Option<Instance<GraphicsSettings, Shared>>,
	input_map: Option<Instance<InputMapSettings, Shared>>,
	audio: Option<Instance<AudioSettings, Shared>>,
}

#[methods]
impl Settings {
	fn new(_owner: &Resource) -> Self {
		Self::default()
	}
}



#[derive(NativeClass, Default, Debug)]
#[inherit(Resource)]
pub(crate) struct InputMapSettings {
	#[property]
	pub input_map: Dictionary,
}

#[methods]
impl InputMapSettings {
	pub fn new(_owner: &Resource) -> Self {
		Self::default()
	}
}

#[cfg(debug_assertions)]
pub const GRAPHICS_SETTINGS_SAVE_PATH: &str = "res://test_saves/saved_graphics_settings.tres";
#[cfg(not(debug_assertions))]
pub const GRAPHICS_SETTINGS_SAVE_PATH: &str = "user://settings/graphics.res";


#[derive(NativeClass, Default, Debug)]
#[inherit(Resource)]
#[register_with(Self::register_properties)]
pub(crate) struct GraphicsSettings;

#[methods]
impl GraphicsSettings {
	
	pub fn new(_owner: &Resource) -> Self {
		Self::default()
	}
	
	fn register_properties(builder: &ClassBuilder<GraphicsSettings>) {
		builder
			.add_property::<bool>("fullscreen")
			.with_getter(|_, _| OS::godot_singleton().is_window_fullscreen())
			.with_setter(|_, _, value| OS::godot_singleton().set_window_fullscreen(value))
			.done();
	}
	
}

#[derive(NativeClass, Default, Debug)]
#[inherit(Resource)]
pub(crate) struct AudioSettings {
	#[property(default = 0.0)] master_volume: f64,
}

#[methods]
impl AudioSettings {
	
	pub fn new(_owner: &Resource) -> Self {
		Self::default()
	}
	
}