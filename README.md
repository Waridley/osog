# One Shot - One Geode
A sniper game that requires you to aim by sound instead of sight. Currently barely even in Minimum Viable Product state.

[Releases on itch.io](https://waridley.itch.io/osog)